import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination, Controller, Thumbs } from "swiper";
import "swiper/swiper-bundle.css";
import "./styles.css";

import ItemScreen from "./Item";
import data from "./data";

SwiperCore.use([Navigation, Pagination, Controller, Thumbs]);

function HeaderBox(props) {
  return (
    <div
      style={{
        width: "230px",
        height: "96px",
        backgroundColor: "black",
        borderRadius: "25px",
        marginInline: "6px",
        color: "white",
      }}
    >
      <p style={{ textAlign: "center", fontSize: "34px" }}> {props.name} </p>
    </div>
  );
}

export default function App() {
  const slides = [];
  let size_data = data.result.data.all_service_item.length;
  let start = 0;

  for (let i = 0; i < size_data / 4; i += 1) {
    slides.push(
      <SwiperSlide key={`slide-${i}`} tag="li">
        <ItemScreen start={start} />
      </SwiperSlide>
    );
    start = start + 4;
  }

  return (
    <div>
      {/* <div
        style={{
          display: "flex",
          flexDirection: "row",
          marginTop: "10px",
          marginBottom: "10px",
          marginLeft: "80px",
        }}
      >
        <HeaderBox name="CUT/DESIGN" />
        <HeaderBox name="CHEMICAL" />
        <HeaderBox name="SKINS" />
        <HeaderBox name="NAILS" />
      </div> */}

      <h1 style={{ marginLeft: "85px" }}>Top 10 Service</h1>
      <Swiper id="main" tag="section" navigation pagination>
        {slides}
      </Swiper>
    </div>
  );
}
