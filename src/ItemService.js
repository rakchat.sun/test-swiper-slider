import React from "react";
import "./styles.css";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  conService: {
    width: "40%",
    height: "120px",
  },
  conTopTen: {
    position: "relative",
    width: "100%",
    height: "150px",
  },
  circleImage: {
    width: "150px",
    height: "100%",
    borderRadius: "50%",
    position: "absolute",
    zIndex: "2",
  },
  boxContent: {
    backgroundColor: "#fabd23",
    width: "80%",
    right: "0",
    top: "11%",
    height: "78%",
    position: "absolute",
    borderRadius: "25px",
    boxShadow: "1px 2px 10px grey",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    fontSize: "18px",
    wordWrap: "break-word",
    height: "100%",
  },
  btnPayNow: {
    color: "white",
    padding: "2px 18px 2px 18px",
    backgroundColor: "black",
    borderRadius: "25px",
    position: "absolute",
    bottom: "14%",
    right: "10%",
    fontWeight: "bold",
    "&:hover": {
      background: "black",
    },
  },
});

const ItemService = (props) => {
  const classes = useStyles();

  return (
    <Box m={2.5} className={classes.conService}>
      <Box className={classes.conTopTen}>
        <Box className={classes.circleImage}>
          <img
            className={classes.circleImage}
            src="https://jaksestudio.com/wp-content/uploads/2020/03/JAKS-estudio-Womens-Haircut.png"
            alt="new"
          />
        </Box>
        <Box className={classes.boxContent}>
          <Box ml={12} pr={1.25} className={classes.content}>
            <Typography
              variant="h6"
              className="item_name"
              numberoflines={1}
              style={{ fontWeight: "bold" }}
            >
              {props.data.service_name}
            </Typography>
            <Typography style={{ color: "#7E7E7E", fontWeight: "bold" }}>
              ({props.data.category_name})
            </Typography>
            <Box mt={1.75}>
              <Typography variant="h6">{props.data.service_price} ฿</Typography>
            </Box>
            <button className="btnPayNow">Pay Now</button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default ItemService;
