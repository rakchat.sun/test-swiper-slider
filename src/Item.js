import React from "react";
import ItemService from './ItemService'

import "./styles.css";

import data  from './data'


const Items = (props) => {
  const data_service =  data.result.data.all_service_item;
  const itemList = []

  for(let i = props.start; i < props.start+4; i++ ){
    itemList.push(
      <ItemService key={i} data={data_service[i]}/>
    )
  }

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        flexWrap: "wrap",
      }}
    >

      {itemList}

    </div>
  );
};

export default Items;
